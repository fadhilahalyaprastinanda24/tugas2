package com.example.tugas2;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etUsername, etPassword;
    private Button button;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText etUsername = findViewById(R.id.etUsername);
        EditText etPassword = findViewById(R.id.etPassword);
        Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                if(username.equalsIgnoreCase(username) && password.equalsIgnoreCase(password)){
                    Intent login = new Intent(MainActivity.this, halamanAwal.class);
                    login.putExtra("password",password);
                    login.putExtra("username",username);
                    startActivity(login);
                    Toast.makeText(MainActivity.this, "Login Berhasil", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(MainActivity.this, "Username atau Password salah", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}