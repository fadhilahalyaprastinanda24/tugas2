package com.example.tugas2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class halamanAwal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_awal);
        TextView textViewUsername = findViewById(R.id.InUsername);
        TextView textViewPassword = findViewById(R.id.InPassword);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String username = extras.getString("username");
            String password = extras.getString("password");

            textViewUsername.setText("Username: " + username);
            textViewPassword.setText("Password: " + password);
        }

    }
}